#!/bin/sh

# strip all trailing whitespace, then all leading whitespace, then all lines
# starting with '#', then all empty lines. then count the remaining lines.
sed 's/[[:space:]]*$//g; s/^[[:space:]]*//g; s/^#.*$//g; /^$/d' shsort.sh \
| wc -l - \
| cut -d' ' -f1

# note that this script's ELOC is NOT included in the count.
