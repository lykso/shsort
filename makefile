SRCDIR = $(dir $(realpath $(firstword $(MAKEFILE_LIST))))
PREFIX ?= /usr
DESTDIR ?= $(PREFIX)/bin

install:
	cp -a $(SRCDIR)/shsort.sh $(DESTDIR)/shsort

uninstall:
	rm $(DESTDIR)/shsort
